//
//  GameScene.swift
//  Game 2d
//
//  Created by IT-Högskolan on 2015-03-27.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

import SpriteKit
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    enum CollisionCategory:UInt32 {
        case player = 1
        case ground = 2
        case weapon = 4
        case obstacle = 8
        case target = 16
        case nothing = 32
    }
    var score = 0
    var obstacleSelector : Float = 0
    
    var isJumping = false
    var isAttacking = false
    var inTransition = false
    
    let player = SKSpriteNode(color: nil, size: CGSize(width: 106, height: 149))
    let weapon = SKSpriteNode(imageNamed: "sword")
    let background1 = SKSpriteNode(imageNamed: "background")
    let background2 = SKSpriteNode(imageNamed: "background")
    let scoreLabel = SKLabelNode(text: "Score: 0")
    
    let highScoreKey = "highScores"
    var musicPlayer = AVAudioPlayer()
    
    var currentTarget : SKSpriteNode? = nil
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        let scale = size.height/background1.size.height
        background1.size.height = size.height
        background1.size.width = background1.size.width * scale
        background2.size = background1.size
        background1.position = CGPoint(x: background1.frame.width*0.5, y: frame.height*0.5)
        background2.position = CGPoint(x: background1.size.width*1.5, y: frame.height*0.5)
        
        let ground = SKShapeNode(rect: CGRect(x: 0, y: size.height * 0.2, width: size.width, height: size.height*0.05))
        ground.fillColor = .blueColor()
        let roof = SKShapeNode(rect: CGRect(x: 0, y: size.height * 0.8, width: size.width, height: size.height*0.05))
        roof.fillColor = .blueColor()
        
        scoreLabel.position = CGPoint(x: size.width * 0.5, y: size.height * 0.80)
        scoreLabel.fontSize = 70
        scoreLabel.fontColor = UIColor.blackColor()
        scoreLabel.fontName = "AmericanTypewriter-Bold"
        
        player.position = CGPoint(x: size.width * 0.2, y: size.height * 0.5)
        weapon.position = CGPoint(x: player.position.x + (player.size.width*0.3), y: player.position.y + (player.size.width*0.2))
        weapon.xScale = 0.3
        weapon.yScale = 0.3
        weapon.zPosition = 1
        
        player.physicsBody = SKPhysicsBody(rectangleOfSize: player.size)
        //player.physicsBody?.usesPreciseCollisionDetection = true
        //player.physicsBody?.affectedByGravity = true
        player.physicsBody?.categoryBitMask = CollisionCategory.player.rawValue
        player.physicsBody?.collisionBitMask = CollisionCategory.ground.rawValue
        player.physicsBody?.contactTestBitMask = CollisionCategory.ground.rawValue
        player.physicsBody?.linearDamping = 1
        player.physicsBody?.allowsRotation = false
        
        weapon.physicsBody = SKPhysicsBody(rectangleOfSize: weapon.size)
        weapon.physicsBody?.allowsRotation = false
        weapon.physicsBody?.categoryBitMask = CollisionCategory.weapon.rawValue
        weapon.physicsBody?.collisionBitMask = CollisionCategory.nothing.rawValue
        weapon.physicsBody?.contactTestBitMask = CollisionCategory.target.rawValue
        
        
        ground.physicsBody = SKPhysicsBody(edgeLoopFromRect: ground.frame)
        //ground.physicsBody?.affectedByGravity = false
        ground.physicsBody?.dynamic = false
        ground.physicsBody?.categoryBitMask = CollisionCategory.ground.rawValue
        //ground.physicsBody?.collisionBitMask = CollisionCategory.player.rawValue
        ground.physicsBody?.contactTestBitMask = CollisionCategory.player.rawValue
        
        
        self.physicsWorld.gravity = CGVector(dx: 0, dy: -10)
        
        
        self.addChild(background1)
        self.addChild(background2)
        self.addChild(ground)
        //self.addChild(roof)
        self.addChild(scoreLabel)
        self.addChild(player)
        self.addChild(weapon)
        self.physicsWorld.contactDelegate = self
        
        let weaponJoint = SKPhysicsJointPin.jointWithBodyA(weapon.physicsBody, bodyB: player.physicsBody, anchor: weapon.position)
        self.physicsWorld.addJoint(weaponJoint)
        
        let backgroundMovement1 = backgroundAnimation(background1)
        let backgroundMovement2 = backgroundAnimation(background2)
        startBackgroundAnimation(backgroundMovement1)
        background2.runAction(backgroundMovement2)
        
        
        startRunningAnimation()
        
        var url = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("backgroundmusic", ofType: "mp3")!)
        
        var error:NSError?
        musicPlayer = AVAudioPlayer(contentsOfURL: url, error: &error)
        musicPlayer.numberOfLoops = -1
        musicPlayer.prepareToPlay()
        musicPlayer.play()
        
        
        let groundObstacleTimer = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: Selector("spawnGroundObstacle"), userInfo: nil, repeats: true)
        
        //let roofObstacleTimer = NSTimer.scheduledTimerWithTimeInterval(4.5, target: self, selector: Selector("spawnRoofObstacle"), userInfo: nil, repeats: true)
        
        let targetTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: Selector("spawnTarget"), userInfo: nil, repeats: true)
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody = contact.bodyA
        var secondBody = contact.bodyB
        
        if firstBody.categoryBitMask > secondBody.categoryBitMask {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if firstBody.categoryBitMask == CollisionCategory.player.rawValue && secondBody.categoryBitMask == CollisionCategory.obstacle.rawValue {
            println("contact")
            runAction(SKAction.playSoundFileNamed("explosion.mp3", waitForCompletion: false))
            let emitter = SKEmitterNode(fileNamed: "sparkKnight")
            emitter.position = player.position
            emitter.particleBirthRate = 100
            emitter.particleLifetime = 1
            emitter.numParticlesToEmit = 40
            self.addChild(emitter)
            //emitter.targetNode = player
            player.removeFromParent()
            weapon.removeFromParent()
            
            if !inTransition {
                let defaults =  NSUserDefaults.standardUserDefaults()
                println(1)
                if var highScore = defaults.arrayForKey(highScoreKey) as? [Int] {
                    println(2, highScore.count)
                    for var i = 0; i < highScore.count; i++ {
                        if highScore[i] < score {
                            highScore.splice([score], atIndex: i)
                            break
                        }
                    }
                    if highScore.count > 10 {
                        highScore.removeLast()
                    } else {
                        highScore.append(score)
                    }
                    defaults.setObject(highScore, forKey: highScoreKey)
                    println("saved")
                    println(highScore.count)
                } else {
                    let newHighScore = [score]
                    defaults.setObject(newHighScore, forKey: highScoreKey)
                }
            }
            
            if let scene = MenuScene.unarchiveFromFile("GameScene", ofClass: 1) as? MenuScene {
                // Configure the view.
                println("menuscene")
                musicPlayer.stop()
                inTransition = true 
                let skView = self.view!
                scene.scaleMode = .AspectFill
                let transition = SKTransition.fadeWithDuration(NSTimeInterval.abs(3))
                transition.pausesOutgoingScene = false
                skView.presentScene(scene, transition: transition)
            }
        }
        
        if firstBody.categoryBitMask == CollisionCategory.player.rawValue && secondBody.categoryBitMask == CollisionCategory.ground.rawValue {
                println("touching ground")
                isJumping = false
        }
        
        if firstBody.categoryBitMask == CollisionCategory.weapon.rawValue && secondBody.categoryBitMask == CollisionCategory.target.rawValue && isAttacking {
            println("weapon + target")
            runAction(SKAction.playSoundFileNamed("shatter.mp3", waitForCompletion: false))
            let emitter = SKEmitterNode(fileNamed: "sparkTarget")
            emitter.position = secondBody.node!.position
            emitter.particleBirthRate = 80
            emitter.particleLifetime = 0.2
            emitter.numParticlesToEmit = 30
            self.addChild(emitter)
            secondBody.node?.removeFromParent()
            currentTarget = nil
            score++
            scoreLabel.text = "Score: \(score)"
        }
    }
    
    func spawnTarget() {
        currentTarget = SKSpriteNode(imageNamed: "target")
        currentTarget?.xScale = 0.5
        currentTarget?.yScale = 0.5
        currentTarget?.physicsBody = SKPhysicsBody(circleOfRadius: currentTarget!.size.width/2)
        currentTarget?.physicsBody?.affectedByGravity = false
        currentTarget?.physicsBody?.categoryBitMask = CollisionCategory.target.rawValue
        currentTarget?.physicsBody?.collisionBitMask = CollisionCategory.ground.rawValue
        currentTarget?.position = CGPoint(x: size.width+100, y: size.height * 0.55)
        
        let move = SKAction.moveToX(-150, duration: 3)
        let action = SKAction.sequence([move, SKAction.runBlock({self.currentTarget!.removeFromParent()})])
        
        self.addChild(currentTarget!)
        currentTarget?.runAction(action)
    }
    
    func spawnGroundObstacle() {
        var sprite = SKSpriteNode(imageNamed: "smallObstacle")
        switch obstacleSelector {
        case 1...3,5...8:
            sprite = SKSpriteNode(imageNamed: "smallObstacle")
        case 4,9:
            sprite = SKSpriteNode(imageNamed: "largeObstacle")
        default:
            break
        }
        obstacleSelector++
        sprite.physicsBody = SKPhysicsBody(texture: sprite.texture, size: sprite.size)
        sprite.physicsBody?.categoryBitMask = CollisionCategory.obstacle.rawValue
        sprite.physicsBody?.contactTestBitMask = CollisionCategory.player.rawValue
        sprite.physicsBody?.dynamic = false
        
        sprite.xScale = 0.7
        sprite.yScale = 0.7
        sprite.position = CGPoint(x: size.width+100, y: (size.height * 0.25 + sprite.frame.height/2))
        
        let move = SKAction.moveToX(-150, duration: 2)
        let action = SKAction.sequence([move, SKAction.runBlock({sprite.removeFromParent()})])
        self.addChild(sprite)
        sprite.runAction(action)
        
        
    }
    
    func spawnRoofObstacle() {
        let sprite = SKSpriteNode(imageNamed:"bomb")
        sprite.physicsBody = SKPhysicsBody(texture: sprite.texture, size: sprite.size)
        sprite.physicsBody?.categoryBitMask = CollisionCategory.obstacle.rawValue
        sprite.physicsBody?.contactTestBitMask = CollisionCategory.player.rawValue
        sprite.physicsBody?.dynamic = false
        
        sprite.xScale = 0.4
        sprite.yScale = 0.4
        sprite.position = CGPoint(x: size.width+100, y: size.height * 0.75)
        let move = SKAction.moveToX(-150, duration: 2)
        let action = SKAction.sequence([move, SKAction.runBlock({sprite.removeFromParent()})])
        self.addChild(sprite)
        sprite.runAction(action)
    }
    
    func startBackgroundAnimation(completeAction: SKAction) {
        let moveLeft = SKAction.moveToX(-(background1.size.width * 0.5), duration: 2.5)
        let action = SKAction.sequence([moveLeft, completeAction])
        background1.runAction(action)
    }
    
    func backgroundAnimation(background: SKSpriteNode) -> SKAction {
        let moveLeft = SKAction.moveToX(-(background1.size.width * 0.5), duration: 5)
        let combinedActions = SKAction.sequence([SKAction.runBlock({background.position = CGPoint(x: background.frame.width*0.5 + background.size.width, y: self.frame.height*0.5)}), moveLeft])
        let action = SKAction.repeatActionForever(combinedActions)
        return action
    }
    
    func startRunningAnimation() {
        var frames : NSMutableArray = []
        
        let images = SKTextureAtlas(named: "run")
        
        for i in 1...images.textureNames.count {
            let texture = images.textureNamed("run\(i).png")
            frames.addObject(texture)
        }
        
        let runAction = SKAction.animateWithTextures(frames, timePerFrame: 0.05)
        let action = SKAction.repeatActionForever(runAction)
        
        player.runAction(action, withKey: "run")
        
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            //let location = touch.locationInNode(self)
            
            if let p = player.physicsBody {
                if !isJumping {
                    println("applying impulse")
                    player.physicsBody!.applyImpulse(CGVector(dx: 0, dy: 700), atPoint: player.position)
                    isJumping = true
                } else {
                    //attack
                    isAttacking = true
                    println("isAttacking: \(isAttacking)")
                    runAction(SKAction.playSoundFileNamed("sword.mp3", waitForCompletion: false))
                    let swingForward = SKAction.rotateToAngle(-2, duration: 0.1)
                    let swingBack = SKAction.rotateToAngle(0, duration: 0.1)
                    let swing = SKAction.sequence([SKAction.runBlock({if self.currentTarget != nil {
                        if self.weapon.intersectsNode(self.currentTarget!) {
                            self.runAction(SKAction.playSoundFileNamed("shatter.mp3", waitForCompletion: false))
                            let emitter = SKEmitterNode(fileNamed: "sparkTarget")
                            emitter.position = self.currentTarget!.position
                            emitter.particleBirthRate = 80
                            emitter.particleLifetime = 0.2
                            emitter.numParticlesToEmit = 30
                            self.addChild(emitter)
                            self.currentTarget?.removeFromParent()
                            self.currentTarget = nil
                            self.score++
                            self.scoreLabel.text = "Score: \(self.score)"
                        }
                        }}), swingForward, swingBack, SKAction.runBlock({self.isAttacking = false; println("isAttacking: \(self.isAttacking)")})])
                    weapon.runAction(swing)
                }
            } else {
                println("no physics body")
            }
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
