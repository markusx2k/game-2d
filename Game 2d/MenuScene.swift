//
//  MenuScene.swift
//  Game 2d
//
//  Created by IT-Högskolan on 2015-03-31.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

import SpriteKit

class MenuScene: SKScene {
    let startButton = SKSpriteNode(imageNamed: "playButton")
    
    override func didMoveToView(view: SKView) {
        
        startButton.position = CGPoint(x: self.size.width*0.5, y: self.size.height*0.5)
        startButton.zPosition = 1
        let background = SKSpriteNode(imageNamed: "background")
        let screen = UIScreen.mainScreen().bounds.size
        println(size.height/background.size.height)
        let scale = size.height/background.size.height
        background.size.height = size.height
        background.size.width = background.size.width * scale
        
        
        background.position = CGPoint(x: self.size.width*0.5, y: self.size.height*0.5)
        self.addChild(background)
        self.addChild(startButton)
        
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            
            if startButton.containsPoint(location) {
                if let scene = GameScene.unarchiveFromFile("GameScene", ofClass: 2) as? GameScene {
                    // Configure the view.
                    runAction(SKAction.playSoundFileNamed("click.mp3", waitForCompletion: false))
                    let skView = self.view!
                    scene.scaleMode = .AspectFill
                    let transition = SKTransition.fadeWithDuration(NSTimeInterval.abs(2))
                    skView.presentScene(scene, transition: transition)
                }
            }
        }
    }

}