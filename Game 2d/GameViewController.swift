//
//  GameViewController.swift
//  Game 2d
//
//  Created by IT-Högskolan on 2015-03-27.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

import UIKit
import SpriteKit

extension SKNode {
    class func unarchiveFromFile(file : NSString, ofClass: Int) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            var scene : SKScene? = nil
            if ofClass == 1 {
                archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
                scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as MenuScene
                archiver.finishDecoding()
            } else if ofClass == 2 {
                archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
                scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as GameScene
                archiver.finishDecoding()
            }
            return scene
        } else {
            return nil
        }
    }
}

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let scene = MenuScene.unarchiveFromFile("GameScene", ofClass: 1) as? MenuScene {
            // Configure the view.
            let skView = self.view as SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            skView.showsPhysics = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            skView.presentScene(scene)
        }
        
//        let skView = self.view as SKView
//        let menu = GameScene()
//        menu.scaleMode = .AspectFill
//        skView.showsFPS = true
//        skView.showsNodeCount = true
//        skView.showsPhysics = true
//        skView.ignoresSiblingOrder = true
//        skView.presentScene(menu)
    }
    
    
    

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
        } else {
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
